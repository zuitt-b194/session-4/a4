-- find all artist that has letter d in its name

SELECT * FROM artists WHERE name LIKE "%d%";


-- find all songs that has a length of less < 230

SELECT * FROM songs WHERE length < 230;


-- Join the albums and songs table. Show album_name, song_name, length

SELECT album_title, song_name, length FROM albums 
    JOIN songs ON albums.id = songs.album_id;


-- Join artists and albums table. Find all albums that has letter a in its name

SELECT * FROM albums WHERE album_title LIKE "%a%";

SELECT * FROM artists   
    JOIN albums on artists.id = albums.artist_id;


-- Sort albums z-a order. show only first 4 records

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;



--Join albums and songs tables. Sort albums from z-a and sort songs from A-Z

SELECT * FROM albums    
    JOIN songs ON albums.id = songs.album_id;

SELECT * FROM albums ORDER BY album_title DESC;
SELECT * FROM songs ORDER BY song_name ASC;